import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperetureController } from './tempereture/tempereture.controller';
import { temperetureService } from './tempereture/tempereture.service';
import { temperetureModule } from './tempereture/tempereture.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [temperetureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
