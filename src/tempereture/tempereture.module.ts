import { Module } from '@nestjs/common';
import { TemperetureController } from './tempereture.controller';
import { temperetureService } from './tempereture.service';

@Module({
  imports: [],
  exports: [],
  controllers: [TemperetureController],
  providers: [temperetureService],
})
export class temperetureModule {}
